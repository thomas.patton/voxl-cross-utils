#!/bin/bash

REBUILD=false
while [[ $# -gt 0 ]]; do
  case "$1" in
    --rebuild | -r)
      REBUILD=true
      ;;
    *)
      # Handle unknown options
      echo "Unknown option: $1"
      exit 1
      ;;
  esac
  shift
done

if [ "$REBUILD" = true ]; then
    echo "Rebuilding libmodal-cv..."
    cd libmodal-cv
    ./clean.sh && ./install_build_deps.sh qrb5165 staging && bear ./build.sh qrb5165 
    ./make_package.sh
    cd ..
fi

echo "Making voxl-feature-tracker..."
current_directory=$(pwd)
echo "Current directory: $current_directory"
cd voxl-feature-tracker
current_directory=$(pwd)
echo "Current directory: $current_directory"
if [ "$REBUILD" = true ]; then
  ./clean.sh && ./install_build_deps.sh qrb5165 staging && bear ./build.sh qrb5165
  cd ..
fi
current_directory=$(pwd)
echo "Current directory: $current_directory"

dpkg -i ./libmodal-cv/libmodal-cv_0.5.0_arm64.deb
current_directory=$(pwd)
echo "Current directory: $current_directory"

cd voxl-feature-tracker && ./make_package.sh

dpkg -i ./voxl-feature-tracker_0.3.4_arm64.deb
echo "New feature tracker created!"

